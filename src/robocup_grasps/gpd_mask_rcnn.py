import numpy as np
import rospy
import actionlib
import tf
import tf2_ros
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from ycb_mask_rcnn import *
from visualization_msgs.msg import Marker
from tf.transformations import quaternion_from_matrix, euler_from_quaternion, quaternion_from_euler
from gpd.msg import CloudIndexed, GraspConfig, GraspConfigList
from geometry_msgs.msg import Quaternion, PoseStamped, Pose, Point
from std_msgs.msg import Header
from moveit_msgs.msg import Grasp
from spherical_grasps_server import SphericalGrasps
from execute_grasp_moveit_grasps.msg import ExecGraspAction, ExecGraspGoal, ExecPlaceAction, ExecPlaceGoal
from control_msgs.msg import JointTrajectoryControllerState

# Get moveit error codes
from moveit_msgs.msg import MoveItErrorCodes
moveit_error_dict = {}
for name in MoveItErrorCodes.__dict__.keys():
	if not name[:1] == '_':
		code = MoveItErrorCodes.__dict__[name]
		moveit_error_dict[code] = name


marker_pub = rospy.Publisher('/simple_grasps', Marker, queue_size = 100)
head_cmd = rospy.Publisher('/head_controller/command', JointTrajectory, queue_size=1)

def scan_head():
    #Pull current head position
    start_state = rospy.wait_for_message('/head_controller/state', JointTrajectoryControllerState)
    start_state = start_state.actual.positions

    time = np.arange(-8.0, 8.0, 0.2)
    amplitude = np.sin(time)
    
    jt = JointTrajectory()
    jt.joint_names = ['head_1_joint', 'head_2_joint']
    duration = 1.0
    for idx in range(0, len(time)):
        jtp = JointTrajectoryPoint()
        jtp.positions = [amplitude[idx], time[idx]/8]
        jtp.time_from_start = rospy.Duration(duration)
        duration += 0.1
        jt.points.append(jtp)
    jtp = JointTrajectoryPoint()
    jtp.positions = start_state
    jtp.time_from_start = rospy.Duration(duration)
    jt.points.append(jtp)
    head_cmd.publish(jt)
    rospy.sleep(15.0) #wait for robot to assume pose

#converts a list of poses into moveit_msgs/Grasp
def poses_to_moveit_grasps(poses):
    #setup spherical grasps server for easy creation of moveit grasps
    sg = SphericalGrasps()
    grasps = []
    for idx, pose in enumerate(poses):
        grasps.append(sg.create_grasp(pose, "grasp_" + str(idx)))
    return grasps


#converts all grasps in gpd/GraspConfigList into moveit grasps
def convert_grasps(grasps):
    #connect to pickup server
    pick_as = actionlib.SimpleActionClient('/pickup_server', ExecGraspAction)

    poses = []
    for grasp in grasps.grasps:
        poses.append(gpd_to_pose(grasp))
        
    grasps = poses_to_moveit_grasps(poses)
    
    for idx, grasp in enumerate(grasps):
        grasp.grasp_pose.header.frame_id = "xtion_depth_optical_frame"
        
        #offset grasp backwards
        point = Point()
        
        offset_vector = np.zeros(4)
        offset_vector[0] = -0.15
        offset_vector[1] = 0
        offset_vector[2] = 0
        offset_vector[3] = 1
        
        trans = tf.TransformerROS()
        translation = (grasp.grasp_pose.pose.position.x, grasp.grasp_pose.pose.position.y, grasp.grasp_pose.pose.position.z)
        rotation = (grasp.grasp_pose.pose.orientation.x, grasp.grasp_pose.pose.orientation.y, grasp.grasp_pose.pose.orientation.z, grasp.grasp_pose.pose.orientation.w)
        R = trans.fromTranslationRotation(translation, rotation)
        
        new_position = R.dot(offset_vector)
        
        point.x = new_position[0]
        point.y = new_position[1]
        point.z = new_position[2]
        
        q = grasp.grasp_pose.pose.orientation
        euler_angles = euler_from_quaternion([q.x, q.y, q.z, q.w])
        print euler_angles
        euler_list = list(euler_angles)
        euler_list[0] -= np.pi/2
        euler_angles = (euler_list[0], euler_list[1], euler_list[2])
        print euler_angles
        new_orientation = quaternion_from_euler(*euler_angles)
        
        new_quaternion = Quaternion()
        new_quaternion.x = new_orientation[0]
        new_quaternion.y = new_orientation[1]
        new_quaternion.z = new_orientation[2]
        new_quaternion.w = new_orientation[3]
        
        #print euler_angles
        grasp.grasp_pose.pose.orientation = new_quaternion
        grasp.grasp_pose.pose.position = point
        
        visualise_pose(grasp.grasp_pose.pose, idx)

    #print grasps[0]
    print "picking up mr mustard"
    pick_g = ExecGraspGoal()
    pick_g.grasps = grasps
    pick_as.wait_for_server()
    pick_as.send_goal_and_wait(pick_g)
    result = pick_as.get_result()
    return result


#converts a gpd GraspConfig message to a geometry_msgs/Pose
def gpd_to_pose(gc):
    approach = np.array([gc.approach.x, gc.approach.y, gc.approach.z, 0])
    binormal = np.array([gc.binormal.x, gc.binormal.y, gc.binormal.z, 0])
    axis = np.array([gc.axis.x, gc.axis.y, gc.axis.z, 0])
    top = np.array([gc.top.x, gc.top.y, gc.top.z, 1])
    R = np.stack((approach, binormal, axis, top), axis=-1)

    q = quaternion_from_matrix(R)
    Q = Quaternion()
    Q.x = q[0]
    Q.y = q[1]
    Q.z = q[2]
    Q.w = q[3]
    pose = Pose()
    pose.orientation = Q
    pose.position.x = gc.bottom.x
    pose.position.y = gc.bottom.y
    pose.position.z = gc.bottom.z
    
    return pose

def visualise_pose(pose, id):
    vis_pose = Marker()
    vis_pose.pose = pose
    vis_pose.header.frame_id = "xtion_depth_optical_frame"
    vis_pose.id = id
    vis_pose.type = Marker.CUBE
    vis_pose.action = Marker.ADD
    vis_pose.scale.x = 0.05
    vis_pose.scale.y = 0.1
    vis_pose.scale.z = 0.03
    vis_pose.color.g = 1.0
    vis_pose.color.a = 1.0
    vis_pose.lifetime = rospy.Duration()
    marker_pub.publish(vis_pose)

def main(pclmsg):
    import rospy
    import ros_numpy
    import cv2

    import rospkg
    #MODEL_PATH = os.path.join(rospkg.RosPack().get_path('execute_grasp_moveit_grasps'), 'src/robocup_grasps/robocup.weights')
    #print MODEL_PATH, rospkg.RosPack().get_path('robocup_go_and_get_it')
    
    pub = rospy.Publisher('/detect_grasps/cloud_indexed', CloudIndexed, queue_size=1)
    #grasp_sub = rospy.Subscriber('/detect_grasps/clustered_grasps', GraspConfigList, convert_grasps)
    rospy.sleep(2)

    rospy.init_node("gpd_grasp", anonymous=True)
    
    scan_head()
    # exit()
    
    #mask_rcnn = YcbMaskRCNN(MODEL_PATH, YCB_LABELS_FULL)

    #pclmsg = rospy.wait_for_message('/xtion/depth_registered/points', PointCloud2)

    #frame, pcl, boxes, clouds, scores, labels, labels_text, masks = mask_rcnn.detect(pclmsg)

    # target = None
    # for i, label in enumerate(labels_text):
    #     if scores[i] > 0.5:
    #         print label
    #         if 'mustard' in label:
    #             target = i

    # if target is not None:
        #pclmsg = clouds[target]
    pcl = np.fromstring(pclmsg.data, dtype=np.float32)
    pcl = pcl.reshape(pclmsg.height, pclmsg.width, -1)

    cloud = np.zeros(pclmsg.height, dtype=[
        ('x', np.float32),
        ('y', np.float32),
        ('z', np.float32)
    ])
    cloud['x'] = pcl[:,:,0].flatten()
    cloud['y'] = pcl[:,:,1].flatten()
    cloud['z'] = pcl[:,:,2].flatten()
    

    #add collision object for target to scene
    # create moveit collision object
    from moveit_msgs.msg import CollisionObject
    from shape_msgs.msg import SolidPrimitive
    from geometry_msgs.msg import Pose

    co_pub = rospy.Publisher('/collision_object', CollisionObject, queue_size=100)
    rospy.sleep(1)

    co = CollisionObject()
    co.header = pclmsg.header
    co.id = 'object'
    co.operation = CollisionObject.ADD

    primitive = SolidPrimitive()
    primitive.type = primitive.BOX
    primitive.dimensions = [0.02, 0.02, 0.02]

    no_samples = 200
    indices = np.random.randint(0, cloud.shape[0], no_samples)
    for i in indices:
        x,y,z = cloud[i]
        primitive_pose = Pose()
        primitive_pose.position.x = x
        primitive_pose.position.y = y
        primitive_pose.position.z = z
        primitive_pose.orientation.x = 0
        primitive_pose.orientation.y = 0
        primitive_pose.orientation.z = 0
        primitive_pose.orientation.w = 1
        co.primitives.append(primitive)
        co.primitive_poses.append(primitive_pose)

    co_pub.publish(co)
    
    # Publish point cloud and nonplanar indices.
    from std_msgs.msg import Header, Int64
    from geometry_msgs.msg import Point

    msg = CloudIndexed()
    header = Header()
    header.frame_id = "xtion_depth_optical_frame"
    header.stamp = rospy.Time.now()
    msg.cloud_sources.cloud = ros_numpy.msgify(PointCloud2, cloud)
    msg.cloud_sources.view_points.append(Point(0,0,0))
    for i in xrange(cloud.shape[0]):
        msg.cloud_sources.camera_source.append(Int64(0))
        msg.indices.append(Int64(i))

    pub.publish(msg)
    print 'Published cloud with', len(msg.indices), 'indices'

    result = convert_grasps(rospy.wait_for_message('/detect_grasps/clustered_grasps', GraspConfigList))
    
    if str(moveit_error_dict[result.error_code]) != "SUCCESS":
        return False
    else:
        return True

if __name__ == '__main__':
    main()

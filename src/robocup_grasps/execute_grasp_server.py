#!/usr/bin/python
import rospy
from actionlib import SimpleActionClient, SimpleActionServer
from moveit_commander import PlanningSceneInterface
from moveit_msgs.msg import Grasp, PickupAction, PickupGoal, PickupResult, MoveItErrorCodes
from moveit_msgs.msg import PlaceAction, PlaceGoal, PlaceResult, PlaceLocation
from geometry_msgs.msg import Pose, PoseStamped, PoseArray, Vector3Stamped, Vector3, Quaternion
from execute_grasp_moveit_grasps.msg import *

class ObjectInteractionServer:
    def __init__(self):
        #get clients for picking and placing actions
        self.pick_ac = SimpleActionClient("/pickup", PickupAction)
        self.pick_ac.wait_for_server()
        self.place_ac = SimpleActionClient("/place", PlaceAction)
        self.place_ac.wait_for_server()

        #start servers for taking pick and place requests
        self.pick_as = SimpleActionServer("/pickup_server", ExecGraspAction,
                execute_cb = self.pick, auto_start = False)
        self.pick_as.start()
        self.place_as = SimpleActionServer("/place_server", ExecPlaceAction,
                execute_cb = self.place, auto_start = False)
        self.place_as.start()
        self.scene = PlanningSceneInterface()

        rospy.loginfo("Grasp server ready.")


    def make_pick_goal(self, group, grasps):
        """
            Makes a PickupGoal.

            Arguments:
                string: joint group with which to plan motion
                moveit_msgs/PlaceLocation[]: list of PlaceLocations to attempt
            
            Returns:
                moveit_msgs/PlaceGoal: goal to attempt placing object at defined positions
        """
        pick_goal = PickupGoal()
        pick_goal.target_name = "object"
        pick_goal.group_name = group
        pick_goal.possible_grasps = grasps
        pick_goal.allowed_planning_time = 45.0
        pick_goal.planning_options.planning_scene_diff.is_diff = True
        pick_goal.planning_options.planning_scene_diff.robot_state.is_diff = True
        pick_goal.planning_options.plan_only = False
        pick_goal.planning_options.replan = True
        pick_goal.planning_options.replan_attempts = 5
        pick_goal.allowed_touch_objects = []
        return pick_goal


    def make_place_goal(self, group, positions):
        """
            Makes a PlaceGoal.

            Arguments:
                string: joint group with which to plan motion
                moveit_msgs/PlaceLocation[]: list of PlaceLocations to attempt
            
            Returns:
                moveit_msgs/PlaceGoal: goal to attempt placing object at defined positions
        """
        place_goal = PlaceGoal()
        place_goal.attached_object_name = "object"
        place_goal.group_name = group
        place_goal.place_locations = positions
        place_goal.allowed_planning_time = 20.0
        place_goal.planning_options.planning_scene_diff.is_diff = True
        place_goal.planning_options.planning_scene_diff.robot_state.is_diff = True
        place_goal.planning_options.plan_only = False
        place_goal.planning_options.replan = True
        place_goal.planning_options.replan_attempts = 2
        return place_goal   


    def pick(self, goal):
        """
            Makes a PickupGoal and attempts to execute the grasps in goal.

            Arguments:
                execute_grasp_moveit_grasps/ExecGraspGoal: goal containing grasps
                to be executed
            
            Returns:
                int: moveit error code (i.e. success/failure)
        """
        #make sure no previously attached object
        self.scene.remove_attached_object("arm_tool_link")
        pick_goal = self.make_pick_goal("arm_torso", goal.grasps)
        self.pick_ac.send_goal_and_wait(pick_goal)
        err = self.pick_ac.get_result().error_code.val
        sg_res = ExecGraspResult()
        sg_res.error_code = err
        if err == 1:
            self.pick_as.set_succeeded(sg_res)
        else:
            self.pick_as.set_aborted(sg_res)


    def place(self, goal):
        """
            Makes a PlaceGoal and attempts to execute the placings in goal.

            Arguments:
                execute_grasp_moveit_grasps/ExecPlaceGoal: goal containing grasps
                to be executed
            
            Returns:
                int: moveit error code (i.e. success/failure)
        """
        place_goal = self.make_place_goal("arm_torso", goal.placings)
        self.place_ac.send_goal_and_wait(place_goal)
        err = self.place_ac.get_result().error_code.val
        if err == 0:
            err = 1
        sg_res = ExecGraspResult()
        sg_res.error_code = err
        if err == 1:
            self.place_as.set_succeeded(sg_res)
        else:
            self.place_as.set_aborted(sg_res)


if __name__ == "__main__":
    rospy.init_node('moveit_grasps_server')
    grasps = ObjectInteractionServer()
    rospy.spin()

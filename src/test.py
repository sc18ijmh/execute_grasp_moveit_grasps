from robocup_grasps import main
from robocup_grasps.ycb_mask_rcnn import *
from gpd.msg import CloudIndexed, GraspConfig, GraspConfigList
import rospkg
import rospy

if __name__ == "__main__":
	MODEL_PATH = os.path.join(rospkg.RosPack().get_path('execute_grasp_moveit_grasps'), 'src/robocup_grasps/robocup.weights')
	print MODEL_PATH, rospkg.RosPack().get_path('robocup_go_and_get_it')
    
	rospy.init_node("gpd_grasp", anonymous=True)

	mask_rcnn = YcbMaskRCNN(MODEL_PATH, YCB_LABELS_FULL)

	pclmsg = rospy.wait_for_message('/xtion/depth_registered/points', PointCloud2)

	frame, pcl, boxes, clouds, scores, labels, labels_text, masks = mask_rcnn.detect(pclmsg)

	target = None
	for i, label in enumerate(labels_text):
		if scores[i] > 0.5:
			print label
			if 'mustard' in label:
				target = i

	if target is not None:
		pclmsg = clouds[target]

	main(pclmsg)
